from datetime import timedelta

from fastapi import APIRouter, Depends

import tasks
import test_task
from app.endpoints.deps import s3_auth
from botocore.client import BaseClient

from app.services.services import redis_pool, queue_0

router = APIRouter()

# https://stackoverflow.com/questions/71984078/fastapi-application-as-an-aws-lambda-function-url-gets-stuck-in-eternal-redirect
@router.get("")
# @router.get("/")
def get_buckets(s3: BaseClient = Depends(s3_auth)):
    print(f"BEFORE {redis_pool.keys()}")
    response = s3.list_buckets()
    # job = queue_0.enqueue(count_words_at_url, 'http://nvie.com')
    queue_tasks()
    print(f"AFTER {redis_pool.keys()}")
    print(f"AFTER {redis_pool.keys()}")
    project = 'TestProject'
    result = {
        'project': project,
        'buckets': response['Buckets']
    }
    str("hh".upper())
    return result

def queue_tasks():
    queue_0.enqueue(tasks.print_task, 5)
    queue_0.enqueue_in(timedelta(seconds=10), tasks.print_numbers, 5)
