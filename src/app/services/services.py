from queue import Queue

import redis
from app.settings import settings
from rq import Queue

# pool = redis.ConnectionPool()
pool = redis.ConnectionPool(host=settings.REDIS_URL, port=6379, db=0)

redis_pool = redis.Redis(connection_pool=pool)
redis_pool.auth(settings.REDIS_KEY)

# q = Queue(connection=redis.Redis())
queue_0 = Queue(connection=redis_pool)
