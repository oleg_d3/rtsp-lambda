#!/usr/bin/python

def upper_case(input_str: str):
    return input_str.upper()

import sys
if __name__ == "__main__":

    if len (sys.argv) > 1:
        print(upper_case(sys.argv[1]))
    else:
        print("$$ NO PARAMS @@")
