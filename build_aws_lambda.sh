#!/bin/sh
docker build -t rtsp-lambda:latest .
docker tag  rtsp-lambda:latest 940523869186.dkr.ecr.us-east-2.amazonaws.com/rtsp-lambda
docker push 940523869186.dkr.ecr.us-east-2.amazonaws.com/rtsp-lambda
aws lambda update-function-code --region us-east-2 --function-name rtsp-lambda-func \
    --image-uri 940523869186.dkr.ecr.us-east-2.amazonaws.com/rtsp-lambda:latest
