# Description

## Docker to AWS

### Create repo
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 940523869186.dkr.ecr.us-east-2.amazonaws.com    
aws ecr create-repository --repository-name rtsp-lambda --image-scanning-configuration scanOnPush=true --image-tag-mutability MUTABLE
### Create docker image and (optional) start locally
docker build -t rtsp-lambda:latest .
docker run rtsp-lambda:latest -p 8002:8000 
### Docker push to AWS ECR
docker tag  rtsp-lambda:latest 940523869186.dkr.ecr.us-east-2.amazonaws.com/rtsp-lambda
docker push 940523869186.dkr.ecr.us-east-2.amazonaws.com/rtsp-lambda
aws lambda update-function-code --region us-east-2 --function-name rtsp-lambda-func \
    --image-uri 940523869186.dkr.ecr.us-east-2.amazonaws.com/rtsp-lambda:latest

~~## Local Testing
### Install runtime locally
mkdir -p ~/.aws-lambda-rie && \
    curl -Lo ~/.aws-lambda-rie/aws-lambda-rie https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie && \
    chmod +x ~/.aws-lambda-rie/aws-lambda-rie

docker run -d -v ~/.aws-lambda-rie:/aws-lambda -p 8002:8000 \
    --entrypoint /aws-lambda/aws-lambda-rie \
    rtsp-lambda:latest \
        /usr/local/bin/python -m awslambdaric main.handler

docker logs <cont_name>~~

## Open-cv links section
https://github.com/dkimg/python-opencv
https://github.com/awslabs/lambda-opencv

## AWS links section
https://pypi.org/project/awslambdaric/
