## Sourse -


ARG FUNCTION_DIR="/app"

FROM public.ecr.aws/docker/library/python:buster as build-image

# Include global arg in this stage of the build
ARG FUNCTION_DIR

# Install aws-lambda-cpp build dependencies
RUN apt-get update && \
  apt-get install -y \
  g++ \
  make \
  cmake \
  unzip \
  libcurl4-openssl-dev \
  gcc wget

#RUN pip install \
#    --target ${FUNCTION_DIR} \
#        awslambdaric

# Copy function code
RUN mkdir -p ${FUNCTION_DIR}
#COPY src/ ${FUNCTION_DIR}/

# Install the function's dependencies
RUN pip install \
    --target ${FUNCTION_DIR} \
        awslambdaric
RUN pip install opencv-python


FROM public.ecr.aws/docker/library/python:buster

# Include global arg in this stage of the build
ARG FUNCTION_DIR
# Set working directory to function root directory
WORKDIR ${FUNCTION_DIR}

# Copy in the built dependencies
COPY --from=build-image ${FUNCTION_DIR} ${FUNCTION_DIR}
EXPOSE 8000
COPY /requirements.txt /
RUN pip install -r /requirements.txt

ADD https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie /usr/bin/aws-lambda-rie

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    TZ=Europe/Belgrade \
    LANG=C.UTF-8 \
    APP_HOME=/app \
    FUNCTION_DIR=/app \
    AWS_SERVER_PUBLIC_KEY=AKIA5V64XKQBHDC4RWWI \
    AWS_SERVER_SECRET_KEY=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 \
    S3_DEFAULT_BUCKET=1st-ipcam-pro


COPY src/ ${FUNCTION_DIR}/


COPY entry.sh /
RUN chmod 755 /usr/bin/aws-lambda-rie /entry.sh
ENTRYPOINT [ "/entry.sh" ]
CMD ["main.handler"]
